<?php
include '../koneksi.php';

$id_dosen = $_POST['id_dosen'];
$nama_dosen   = $_POST['nama_dosen'];
$nip_dosen     = $_POST['nip_dosen'];
$prodi    = $_POST['prodi'];
$fakultas    = $_POST['fakultas'];
$foto_dosen = $_FILES['foto_dosen']['name'];

if ($foto_dosen != "") {
    $ekstensi_diperbolehkan = array('png', 'jpg');
    $x = explode('.', $foto_dosen);
    $ekstensi = strtolower(end($x));
    $file_tmp = $_FILES['foto_dosen']['tmp_name'];
    $angka_acak     = rand(1, 999);
    $nama_gambar_baru = $angka_acak . '-' . $foto_dosen;
    if (in_array($ekstensi, $ekstensi_diperbolehkan) === true) {
        move_uploaded_file($file_tmp, 'gambar/' . $nama_gambar_baru);


        $query  = "UPDATE dosen SET nama_dosen = '$nama_dosen', nip_dosen = '$nip_dosen', prodi = '$prodi', fakultas = '$fakultas', foto_dosen = '$nama_gambar_baru'";
        $query .= "WHERE id_dosen = '$id_dosen'";
        $result = mysqli_query($koneksi, $query);

        if (!$result) {
            die("Query gagal: " . mysqli_errno($koneksi) .
                " - " . mysqli_error($koneksi));
        } else {
            echo "<script>alert('Data Sudah Diubah.');window.location='index.php';</script>";
        }
    } else {
        echo "<script>alert('Gambar dalam bentuk jpg atau png.');window.location='prosesEdit.php';</script>";
    }
} else {
    $query  = "UPDATE dosen SET nama_dosen = '$nama_dosen', nip_dosen = '$nip_dosen', prodi = '$prodi', fakultas = '$fakultas'";
    $query .= "WHERE id_dosen = '$id_dosen'";
    $result = mysqli_query($koneksi, $query);

    if (!$result) {
        die("Query gagal: " . mysqli_errno($koneksi) .
            " - " . mysqli_error($koneksi));
    } else {
        echo "<script>alert('Data Sudah Diubah.');window.location='index.php';</script>";
    }
}