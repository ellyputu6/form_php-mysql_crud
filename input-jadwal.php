<?php
include '../koneksi.php';
?>
<!DOCTYPE html>
<html>

<head>
    <title>JADWAL</title>
    <style type="text/css">
    * {
        font-family: "Trebuchet MS";
    }

    h1 {
        text-transform: uppercase;
        color: black;
    }

    button {
        background-color: green;
        color: #fff;
        padding: 10px;
        text-decoration: none;
        font-size: 12px;
        border: 0px;
        margin-top: 20px;
    }

    label {
        margin-top: 10px;
        float: left;
        text-align: left;
        width: 100%;
        color: black;
    }

    input {
        padding: 6px;
        width: 100%;
        box-sizing: border-box;
        background: white;
        border: 2px solid #ccc;
        outline-color: lightskyblue;
    }

    div {
        width: 100%;
        height: auto;
    }

    .base {
        width: 400px;
        height: auto;
        padding: 20px;
        margin-left: auto;
        margin-right: auto;
        background: darksalmon;
    }
    </style>
</head>

<body>
    <center>
        <h1>Tambah Data</h1>
        <center>
            <form method="POST" action="proses_tambah.php" enctype="multipart/form-data">
                <section class="base">
                    <div>
                        <label>Nama Mata Kuliah</label>
                        <input type="text" name="nama_matkul" autofocus="" required="" />
                    </div>
                    <div>
                        <label>Hari</label>
                        <input type="text" name="hari" required="" />
                    </div>
                    <div>
                        <label>Prodi</label>
                        <input type="text" name="prodi" required="" />
                    </div>
                    <div>
                        <label>Fakultas</label>
                        <input type="text" name="fakultas" required="" />
                    </div>
                    <div>
                        <button type="submit">Simpan</button>
                    </div>
                </section>
            </form>
</body>

</html>