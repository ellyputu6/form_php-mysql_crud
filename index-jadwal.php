<?php

include '../koneksi.php'

?>
<!DOCTYPE html>
<html>

<head>
<!-- Required meta tags -->
<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <title>JADWAL</title>
    
</head>

<body>
<!-- Option 1: Bootstrap Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
    <center>
        <h1>Keseluruhan Data</h1>
        <center>
                <center>
                    <br />
                    <div class="container">
                     <div class="row justify-content-center">
                        <div class="col-9 border mt-3 p-3">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Mata Kuliah</th>
                                <th>Hari</th>
                                <th>Prodi</th>
                                <th>Fakultas</th>
                     
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $query = "SELECT * FROM jadwal ORDER BY id_jadwal ASC";
                            $result = mysqli_query($koneksi, $query);
                           
                            if (!$result) {
                                die("Query Error: " . mysqli_errno($koneksi) .
                                    " - " . mysqli_error($koneksi));
                            }

                            
                            $no = 1; 
                            while ($row = mysqli_fetch_assoc($result)) {
                            ?>
                            <tr>
                                <td><?php echo $no; ?></td>
                                <td><?php echo $row['nama_kelas']; ?></td>
                                <td><?php echo $row['hari']; ?></td>
                                <td><?php echo $row['prodi']; ?></td>
                                <td><?php echo $row['fakultas']; ?></td>
                                  <td>  <a href="edit_jadwal.php?id_jadwal=<?php echo $row['id_jadwal']; ?>"><button type="button" class="btn btn-primary">Edit</button></a> |
                                    <a href="proses_hapus.php?id_jadwal=<?php echo $row['id_jadwal']; ?>"
                                        onclick="return confirm('Anda yakin akan menghapus data ini?')"><button type="button" class="btn btn-danger">Hapus</button></a></td>
                                </td>
                            </tr>

                            <?php
                                $no++;
                            }
                            ?>
                        </tbody>
                    </table>
                    <a href="input.php"><button type="button" class="btn btn-success">Tambah</button></a>
                </div>
          </div>
        </div>
</body>

</html>