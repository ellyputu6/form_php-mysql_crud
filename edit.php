<?php
include '../koneksi.php';

if (isset($_GET['id_dosen'])) {
    $id_dosen = ($_GET["id_dosen"]);


    $query = "SELECT * FROM dosen WHERE id_dosen='$id_dosen'";
    $result = mysqli_query($koneksi, $query);

    if (!$result) {
        die("Query Error: " . mysqli_errno($koneksi) .
            " - " . mysqli_error($koneksi));
    }

    $data = mysqli_fetch_assoc($result);
    if (!count($data)) {
        echo "<script>alert('Data Tidak Ditemukan');window.location='index.php';</script>";
    }
} else {
    echo "<script>alert('Silahkan Masukkan id_dosen.');window.location='index.php';</script>";
}
?>
<!DOCTYPE html>
<html>

<head>
    <title>DOSEN</title>
    <style type="text/css">
    * {
        font-family: "Trebuchet MS";
    }

    h1 {
        text-transform: uppercase;
        color: black;
    }

    button {
        background-color: green;
        color: #fff;
        padding: 10px;
        text-decoration: none;
        font-size: 12px;
        border: 0px;
        margin-top: 20px;
    }

    label {
        margin-top: 10px;
        float: left;
        text-align: left;
        width: 100%;
        color: black;
    }

    input {
        padding: 6px;
        width: 100%;
        box-sizing: border-box;
        background: white;
        border: 2px solid #ccc;
        outline-color: lightskyblue;
    }

    div {
        width: 100%;
        height: auto;
    }

    .base {
        width: 400px;
        height: auto;
        padding: 20px;
        margin-left: auto;
        margin-right: auto;
        background: darksalmon;
    }
    </style>
</head>

<body>
    <center>
    <h1 class="text-center mt-5">Edit Data</h1>
        <center>
            <form method="POST" action="prosesEdit.php" enctype="multipart/form-data">
                <section class="base">
                    <input name="id_dosen" value="<?php echo $data['id_dosen']; ?>" hidden />
                    <div>
                        <label>Nama</label>
                        <input type="text" name="nama_dosen" value="<?php echo $data['nama_dosen']; ?>" autofocus=""
                            required="" />
                    </div>
                    <div>
                        <label>NIP</label>
                        <input type="text" name="nip_dosen" value="<?php echo $data['nip_dosen']; ?>" />
                    </div>
                    <div>
                        <label>Prodi</label>
                        <input type="text" name="prodi" required="" value="<?php echo $data['prodi']; ?>" />
                    </div>
                    <div>
                        <label>Fakultas</label>
                        <input type="text" name="fakultas" required="" value="<?php echo $data['fakultas']; ?>" />
                    </div>
                    <div>
                        <label>Foto</label>
                        <img src="gambar/<?php echo $data['foto_dosen']; ?>"
                            style="width: 120px;float: left;margin-bottom: 5px;">
                        <input type="file" name="foto_dosen" />
                    </div>
                    <div>
                        <button type="submit">Simpan</button>
                    </div>
                </section>
            </form>
</body>

</html>