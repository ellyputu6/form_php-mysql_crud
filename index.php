<?php
include '../koneksi.php';
?>

<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

    <title>DOSEN</title>
</head>

<body>
    <h1 class="text-center mt-5">Keseluruhan Data</h1>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-9 border mt-3 p-3">
                <table class="table table-bordered">
                <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>NIP</th>
                                <th>Prodi</th>
                                <th>Fakultas</th>
                                <th>Foto</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $query = "SELECT * FROM dosen ORDER BY id_dosen ASC";
                            $result = mysqli_query($koneksi, $query);
                            //mengecek apakah ada error ketika menjalankan query
                            if (!$result) {
                                die("Query Error: " . mysqli_errno($koneksi) .
                                    " - " . mysqli_error($koneksi));
                            }

                            $no = 1;
                            while ($row = mysqli_fetch_assoc($result)) {
                            ?>
                            <tr>
                                <td><?php echo $no; ?></td>
                                <td><?php echo $row['nama_dosen']; ?></td>
                                <td><?php echo $row['nip_dosen']; ?></td>
                                <td><?php echo $row['prodi']; ?></td>
                                <td><?php echo $row['fakultas']; ?></td>
                                <td style="text-align: center;"><img src="gambar/<?php echo $row['foto_dosen']; ?>"
                                        style="width: 120px;"></td>
                                <td>
                                    <a href="edit.php?id_dosen=<?php echo $row['id_dosen']; ?>"><button type="button" class="btn btn-primary">Edit</button></a> |
                                    <a href="hapus.php?id_dosen=<?php echo $row['id_dosen']; ?>"
                                        onclick="return confirm('Anda yakin akan menghapus data ini?')"><button type="button" class="btn btn-danger">Hapus</button></a>
                                </td>
                            </tr>

                            <?php
                                $no++;
                            }
                            ?>
                        </tbody> </table>
                <a href="input.php"><button type="button" class="btn btn-success">Tambah</button></a>
            </div>
          </div>
        </div>
            <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
    
    </body>

</html>
