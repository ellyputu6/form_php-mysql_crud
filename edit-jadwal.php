<?php
include '../koneksi.php';

if (isset($_GET['id_jadwal'])) {
    $id_jadwal = ($_GET["id_jadwal"]);
    $query = "SELECT * FROM jadwal WHERE id_jadwal='$id_jadwal'";
    $result = mysqli_query($koneksi, $query);

    if (!$result) {
        die("Query Error: " . mysqli_errno($koneksi) .
            " - " . mysqli_error($koneksi));
    }
    $data = mysqli_fetch_assoc($result);
    if (!count($data)) {
        echo "<script>alert('Data tidak ditemukan pada database');window.location='index.php';</script>";
    }
} else {
    echo "<script>alert('Masukkan data id_kelas.');window.location='index.php';</script>";
}
?>
<!DOCTYPE html>
<html>

<head>
    <title>JADWAL</title>
    <style type="text/css">
    * {
        font-family: "Trebuchet MS";
    }

    h1 {
        text-transform: uppercase;
        color: blue;
    }

    button {
        background-color: blue;
        color: #fff;
        padding: 10px;
        text-decoration: none;
        font-size: 12px;
        border: 0px;
        margin-top: 20px;
    }

    label {
        margin-top: 10px;
        float: left;
        text-align: left;
        width: 100%;
    }

    input {
        padding: 6px;
        width: 100%;
        box-sizing: border-box;
        background: #f8f8f8;
        border: 2px solid #ccc;
        outline-color: blue;
    }

    div {
        width: 100%;
        height: auto;
    }

    .base {
        width: 400px;
        height: auto;
        padding: 20px;
        margin-left: auto;
        margin-right: auto;
        background: #ededed;
    }
    </style>
</head>

<body>
    <center>
        <h1>Edit Jadwal <?php echo $data['nama_matkul']; ?></h1>
        <center>
            <form method="POST" action="proses_edit.php" enctype="multipart/form-data">
                <section class="base">
                    
                    <input name="id_jadwal" value="<?php echo $data['id_jadwal']; ?>" hidden />
                    <div>
                        <label>Nama Mata Kuliah</label>
                        <input type="text" name="nama_matkul" value="<?php echo $data['nama_matkul']; ?>" autofocus=""
                            required="" />
                    </div>
                    <div>
                        <label>Hari</label>
                        <input type="text" name="hari" required="" value="<?php echo $data['hari']; ?>" />
                    </div>
                    <div>
                        <label>Prodi</label>
                        <input type="text" name="prodi" required="" value="<?php echo $data['prodi']; ?>" />
                    </div>
                    <div>
                        <label>Fakultas</label>
                        <input type="text" name="fakultas" required="" value="<?php echo $data['fakultas']; ?>" />
                    </div>
                    <div>
                        <button type="submit">Simpan</button>
                    </div>
                </section>
            </form>
</body>

</html>